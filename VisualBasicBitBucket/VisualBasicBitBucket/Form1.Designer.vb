﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGrow = New System.Windows.Forms.Button()
        Me.btnShrink = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnGrow
        '
        Me.btnGrow.BackColor = System.Drawing.Color.OrangeRed
        Me.btnGrow.Font = New System.Drawing.Font("Niagara Solid", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrow.Location = New System.Drawing.Point(69, 99)
        Me.btnGrow.Name = "btnGrow"
        Me.btnGrow.Size = New System.Drawing.Size(116, 48)
        Me.btnGrow.TabIndex = 0
        Me.btnGrow.Text = "Grow"
        Me.btnGrow.UseVisualStyleBackColor = False
        '
        'btnShrink
        '
        Me.btnShrink.BackColor = System.Drawing.Color.Lime
        Me.btnShrink.Font = New System.Drawing.Font("OCR A Extended", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShrink.Location = New System.Drawing.Point(321, 99)
        Me.btnShrink.Name = "btnShrink"
        Me.btnShrink.Size = New System.Drawing.Size(116, 48)
        Me.btnShrink.TabIndex = 2
        Me.btnShrink.Text = "Shrink"
        Me.btnShrink.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(641, 331)
        Me.Controls.Add(Me.btnShrink)
        Me.Controls.Add(Me.btnGrow)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnGrow As Button
    Friend WithEvents btnShrink As Button
End Class
